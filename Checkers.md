## Checkers

### Game Rule Change Rule

We introduce the new rule called _Swap_

* The king can be swaped with any piece on the board.
* The only condition imposed is that the king should not be on the last row of the other player.
* One chance is consumed in this move
* This rule can be used as many times as possible

### Premise of Rule Change

* After converting normal the piece to king, the player wouldn't have to wait for a few more chances to get the king in position, he can utilize the flagged positions by other pieces on the board
* The original game had somewhat less tricks, so we propose this rule to increase player's arsenal.

### Change of Game Play

- The game becomes more competitive as the king can now land at any of the crucial places on the board at the expense of one move.
- The player can use this strategy to make more kings quickly by swapping pieces.
- This makes the converting a piece to king even more crucial than it was in the original game. So yes, if one player makes one of its pieces king and the other player is no where close to making one, it may  just kill the game.
- Therefore on every swap, one chance is consumed, so the opposite player kind of gets 2 consecutive chances which can be used as an effective counter move. 

#### Player Engagement

* The player takes more interest in the game as he can now place other pieces on the board more strategically so that the new powers of the king can be used more effectively.

#### Convergence of the game

- The convergence of the game remains same as before, as this rule does not alter any fundamental rule, but just brings a new technique into the game

### Time to complete the game

* The game might end quickly than original game, since the user with more kings will significantly high advantage than the amount of advantage he had in the original game over the other player.