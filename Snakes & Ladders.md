## Snakes & Ladders

### Game Rule Change

We introduced a new rule called _Toggle_.

- When a player gets 6 on his dice, he can choose whether to toggle or not. After deciding, he will play that 6.
- Toggle means that the functioning of snakes and ladders will be reversed. The snakes will behave like ladders : one can go from snake's tail to its head. And the ladders will behave like snakes : one will go down from upper end of ladder to its lower end.
- On each toggle, the functioning of snakes and ladders will be reversed.
- There can be two varients of the rule change:
  - Toggle will affect the pieces only after the current move. Thus the pieces which are already at the end of a ladder or a snake, will not be affected by toggle.
  - Toggle will affect all the pieces instantaneously. Hence the the pieces which are already at the end of a ladder or a snake, will be affected according to changed rule.

### Premise of Rule Change

- Snakes and Ladders is purely chance based game. We wanted to introduce some decision making in that. After each 6 on dice, player has a choice. Based on his status on the board, he can decide whether to go for toggle or not.

### Change in Game Play

- Original Snakes & Ladders is purely chance based game. After the rule change, it becomes little strategic because the decision of toggle is in player's hand.
- In the standard Snakes & Ladders board configuration, the last row has 4 snakes so the player faces higher risk when he is at the last row. But he can make himself safe by toggling. But the opponent can toggle the behaviour also. 

## Game Maths

### Expected Number of Moves of One Player to Complete the Game

We ran $1000000$ simulations on several types of snakes and ladders boards. 

[Code File Link](https://gitlab.com/pearlstar/Game-Design-Engineering-Reports)

#### Empty Board With Normal Rules

- No snakes and ladders (Just empty boards)

- Result of simulations : $29.9$ moves

- Player needs $99$ moves to complete the game. Expected number on dice is $3.5$

  $$ E = 99 / 3.5 = 28.29 $$

- Here the difference is due to the rule that player can't go beyond $100$. If dice takes him beyond 100, then he doesn't play that move. Thus it would take slightly longer to complete the end part.

#### Standard Board With Normal Rules (Given in 'snakes_and_ladders_board.png')

![snakes_and_ladders_board](/home/pearlstud/Documents/Game_Design_and_Engineering/Game-Design-Engineering-Reports/snakes_and_ladders_board.png)

- Result of simulations : $27.1$ moves
- Here it is worth to notice that the expected number of moves of this board is near equal to empty board. Thus we can conclude that the configuration of snakes and ladders are very balanced.

#### Standard Board with Changed Rule

- Empty board with changed rule doesn't matter because it won't affect anything so we tested on standard board with rule change.
- We ran simulations. On each dice of $6$, we toggle with probability $0.5$.
- Expected number of moves to complete the game was : $33.2$ moves
- It indicates that the rule change makes the game little longer.





#### Player Engagement

- The game after rule change becomes quite interesting.
- The game will not be just a chance, but it will involve player's decision making of toggle. Hence it will increase player's involvement in the game.

#### Convergence of the Game

- After the rule change, the chances for the convergence of the game will remain almost the same as before. Normal Snakes & Ladders game can go infinite if the players keep getting into the mouth of snakes. This possibility of infinite game remains even after the rule change.

#### Time to Complete the Game

- The expected completion time of the game will remain the same because for a random board configuration, toggle is equivalent to changing the board configuration. 

