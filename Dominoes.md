## Dominoes

### Game Rule Change

We introduce _Reveal_ as a new rule into the game

* For only once in the game, either player can choose to see any one of the domino piece of other player

### Premise of Rule Change

- The game becomes more interesting as the end approaches after applying this rule. It becomes possible to know almost all the pieces of the other player using this rule which makes the game competiting.
- Another motive behind introducing this rule was to improve chances of comeback of losing player as he/she can use this rule very strategically to gain advantage at the end.


### Change in gameplay

- Net effect of the new rule is that it makes the game more strategic and competitive.
- Player tends to keep his reveal power for the end game. So the game becomes very competitive at the end.
- In normal Dominoes, a player tries to estimate opponent's pieces when the opponent doesn't have suitable piece in his stack and picks up a piece from outer stack. Player iteratively builds up his estimate about opponent's pieces. Here the changed rule gives player a way to know one of the opponent's pieces clearly.

#### Player Engagement

- We found the game to be more interesting and competitive when played with changed rule.
- New rule brings surprising twists to an already exciting end part of the game.

#### Convergence of the Game

- Here the changed rule only affects the player's information about the game. The changed rule affects the strategy of the game only. So the convergence of the game will be the same as before.
- There are finite number of pieces and with all pieces, there is always a way to end the game i.e. to arrange the pieces according to rules. Thus the longest game can go only when there is no piece in outer stack. Even in that case, the game will come to an end. Thus it is surely converging game.

#### Time to Complete the Game

- New rule gives player a nice chance to come back. Thus some game goes longer than normal.
- But Dominoes is anyway a very short game and the rule change won't effect the time quite much.