import random
import numpy as np

N = 10

sl = {
    16 : 6,
    49 : 11,
    48 : 26,
    56 : 53,
    64 : 60,
    62 : 19,
    87 : 24,
    93 : 73,
    95 : 75,
    98 : 78,

    1 : 38,
    4 : 14,
    9 : 31,
    21 : 42,
    28 : 84,
    36 : 44,
    51 : 67,
    71 : 91,
    80 : 100
}




max_iter = 50000

total = 0

for it in xrange(max_iter):

    i = 1
    p1 = 1
    p2 = 1

    while True:

        dice = np.random.randint(1, 7)
        old = p1
        p1 += dice
        if sl.has_key(p1):
            p1 = sl[p1]

        if p1 == 100:
            total += i
            break
        if p1 > 100:
            p1 = old

        dice = np.random.randint(1, 7)
        old = p2
        p2 += dice
        if sl.has_key(p2):
            p2 = sl[p2]

        if p2 == 100:
            total += i
            break
        if p2 > 100:
            p2 = old

        i += 1

print 1.0 * total / max_iter
