## Game Design Engineering Reports

### Team Members:
- Chaitanya Patel : 201501071
- Moin Hussain Moti : 201501066

### Report on following games included :
1. Ludo
2. Othello
3. Snakes & Ladders
4. Brainvita
5. Dominoes
6. Checkers
7. Mancala
8. Scrabble
